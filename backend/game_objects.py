import logging
from collections import OrderedDict
from random import shuffle, random
from typing import Dict, List, Set, Optional

from starlette.websockets import WebSocket

from models import User

log = logging.getLogger(__name__)


class Game:
    colors = list(range(1, 9))
    ready: Set['Player']
    players: List['Player']
    name: str
    password: Optional[str]

    class Stages:
        WAIT = 0
        START = 1
        SEND = 2
        VOTES = 3
        END = 4

    def __init__(self, creator: User, name='', password: Optional[str] = None):
        self.name = name or str(random())
        self.password = password
        self.creator = creator

        self.players = []
        self.ready = set()
        self.table = Table(self)

        self.message = ''
        self.cards = self.create_cards()

    @property
    def stage(self):
        if len(self.players) < 3:
            return self.Stages.WAIT
        if not self.message:
            return self.Stages.START
        if len(self.table.cards) < len(self.players):
            return self.Stages.SEND
        if len(self.table.voted_players) < (len(self.players) - 1):
            return self.Stages.VOTES
        return self.Stages.END

    @staticmethod
    def create_cards():
        symbols = ('♣', '♦', '♥', '♠')
        nums = ('a', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'j', 'q', 'k')
        cards = [n + s for n in nums for s in symbols]
        shuffle(cards)
        return cards

    async def send(self, data=None):
        for player in self.players:
            await player.send(data)

    @property
    def player(self):
        if self.players:
            return self.players[0]

    def next(self):
        if len(self.players) > 1:
            self.players.append(self.players.pop(0))

            for player, card in self.table._cards.items():
                player.cards.remove(card)
                self.cards.append(card)

            shuffle(self.cards)
            for player in self.players:
                player.cards.append(self.cards.pop())

    def add_player(self, player: 'Player'):
        if len(self.players) >= 8:
            return False

        for i in self.players:
            if i.user.id == player.user.id:
                return False

        player.game = self
        player.color = self.colors.pop(0)
        self.players.append(player)

        player.cards = [self.cards.pop() for i in range(6)]

        self.restart()

        return True

    def remove_player(self, player: 'Player'):
        self.cards.extend(player.cards)
        self.colors.append(player.color)

        self.players.remove(player)
        self.restart()

    def restart(self):
        self.table.clean()
        self.message = ""
        self.ready = set()

        shuffle(self.cards)

    def end(self):
        inv_cards = {card: player for player, card in self.table._cards.items()}

        for card, players in self.table.votes.items():
            if card == self.table.turn_card:
                for player in players:
                    player.score += 1

                if len(players) == len(self.table.voted_players):
                    self.player.score -= 1
                    return
                continue

            inv_cards[card].score += len(players)

        for player in self.players:
            if player.user:
                player.user.score = player.score

    def __str__(self):
        return f'{self.name}[{len(self.players)}]'


class Player:
    game: Game
    cards: List[str]
    color = 0
    score = 0
    user: User = None

    def __init__(self, ws: WebSocket, user: User = None):
        self.cards = []
        if user:
            self.name = user.name
            self.score = user.score
            self.user = user
        else:
            self.name = f'Player {int(random() * 100)}'
        self.ws = ws

        log.info(f'Player created: {self.name}')

    @property
    def state(self):
        if not self.game:
            return None

        my_turn = self.game.player is self
        table_opened = self.game.table.is_open
        i_am_voted = self in self.game.table.voted_players
        started = bool(self.game.message)

        if my_turn and not started:
            return 1
        if not table_opened and self not in self.game.table and started:
            return 1
        if table_opened and not my_turn and not i_am_voted:
            return 1
        if self.game.stage == Game.Stages.END and self not in self.game.ready:
            return 1

        return None

    def json(self):
        return {
            'score': self.score,
            'name': self.name,
            'color': self.color,
            'state': self.state,
        }

    def payload(self, data=None):
        data = data or {}

        if not self.game:
            return data

        players = []
        for player in self.game.players:
            player_data = player.json()
            if player == self:
                player_data['me'] = True
            if player == self.game.player:
                player_data['turn'] = True
            players.append(player_data)

        result = {
            **data,
            "hand": self.cards,
            "table": self.game.table.cards,
            "voted": [i.json() for i in self.game.table.voted_players],
            "votes": {k: [i.json() for i in v] for k, v in self.game.table.votes.items()},
            "message": self.game.message,
            "players": players,
            "stage": self.game.stage
        }

        return result

    async def send(self, data=None):
        p = self.payload(data or {})
        log.debug(f'=[{self}]> {p}')
        await self.ws.send_json(p)

    async def read(self):
        try:
            message = await self.ws.receive_json()
        except Exception as e:
            log.error(e)
            return {}

        log.debug(f'<[{self}]= {message}')
        return message

    def __str__(self):
        return str(self.name)


class Table:
    _cards: Dict[Player, str]
    _votes: Dict[str, List[Player]]

    def __init__(self, game):
        self.game = game
        self.clean()

    @property
    def is_open(self):
        return len(self._cards) >= len(self.game.players)

    @property
    def voted_players(self):
        players = []
        for i in self._votes.values():
            players.extend(i)
        return players

    def __contains__(self, card: str):
        if isinstance(card, str):
            return card in self._cards.values()
        elif isinstance(card, Player):
            return card in self._cards
        else:
            raise Exception()

    @property
    def cards(self):
        if self.is_open:
            return tuple(self._cards.values())
        else:
            return (None,) * len(self._cards)

    @property
    def votes(self):
        if self.game.stage == Game.Stages.END:
            return self._votes
        else:
            return {}

    @property
    def turn_card(self):
        return self._cards[self.game.player]

    def add_card(self, card: str, player: Player):
        log.info(f'{player} add card {card}')

        self._cards.update({player: card})
        self._votes.update({card: []})

    def vote(self, card: str, player: Player):
        if card not in self._cards.values():
            log.info('Card not on table')
            return False

        if self._cards[player] == card:
            log.info('Prevent table-vote')
            return False

        for c, votes in self._votes.items():
            if player in votes:
                if card == c:
                    return False
                votes.remove(player)
                break

        self._votes[card].append(player)

        return True

    def clean(self):
        self._cards = OrderedDict()
        self._votes = OrderedDict()


async def run(player, game):
    msg = await player.read()
    stage = game.stage

    if not stage:
        return

    if stage == Game.Stages.END:
        game.ready.add(player)

        if len(game.players) == len(game.ready):
            if player.user:
                player.user.score = player.score
                await player.user.save()

            game.next()
            game.restart()

        await game.send()
        return

    if 'card' not in msg:
        return

    my_turn = game.player is player

    if stage == Game.Stages.START and my_turn:
        game.message = msg['message']
        game.table.add_card(msg['card'], player)

        await game.send()
        return

    if stage == Game.Stages.SEND and not my_turn:
        game.table.add_card(msg['card'], player)

        await game.send()
        return

    if stage == Game.Stages.VOTES and not my_turn and msg['card'] in game.table:
        result = game.table.vote(msg['card'], player)

        if result:
            if game.stage == Game.Stages.END:
                game.end()

            await game.send()
