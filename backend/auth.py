import logging

from fastapi import Cookie, HTTPException
from tortoise.exceptions import DoesNotExist

from models import Session

log = logging.getLogger(__name__)


async def get_session(session_id: str = Cookie(None)) -> Session:
    try:
        session = await Session.get(id=session_id).prefetch_related('user')
    except DoesNotExist:
        log.warning(f'No session with id: {session_id}')
        raise HTTPException(status_code=401, detail=f'No session with id: {session_id}')

    return session
