import re
from typing import Optional, List, Dict

from pydantic import BaseModel, ValidationError
from pydantic.error_wrappers import ErrorWrapper


class UserCredentials(BaseModel):
    name: str
    password: str

    @classmethod
    def validate(cls, value):
        obj = super().validate(value)
        errors = []
        if re.sub(r'[0-9A-Za-z_\-.]', '', obj.name):
            errors.append(
                TypeError('Only letters, numbers and _,- symbols available in name')
            )

        if 15 < len(obj.name) < 3:
            errors.append(
                TypeError('Name length must be in range from 3 to 15')
            )

        if not obj.password or len(obj.password) < 6:
            errors.append(
                TypeError('Password cant be less than 6 symbols')
            )

        if errors:
            raise ValidationError([ErrorWrapper(i, loc='__obj__') for i in errors])

        return obj


class LoginResult(BaseModel):
    ok: bool = True
    detail: List[Dict] = []
    session_id: Optional[str]


class GameCredentials(BaseModel):
    name: str
    password: Optional[str]


class ResponseUserModel(BaseModel):
    id: int
    name: str
    score: int = 0


class ResponseUser(BaseModel):
    ok: bool = True
    error: str = ''
    user: Optional[ResponseUserModel]


class ResponseGame(BaseModel):
    id: int
    name: str
    password: bool
    players: int


class ResponseGamesList(BaseModel):
    ok: bool
    games: List[ResponseGame]
