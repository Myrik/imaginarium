import logging
from typing import Dict, Optional

import uvicorn
from fastapi import FastAPI, Depends, HTTPException, Cookie
from fastapi.security import OAuth2PasswordBearer
from starlette.middleware.cors import CORSMiddleware
from starlette.responses import Response
from starlette.status import WS_1008_POLICY_VIOLATION, HTTP_201_CREATED, WS_1013_TRY_AGAIN_LATER, HTTP_403_FORBIDDEN
from starlette.websockets import WebSocket, WebSocketState
from tortoise.exceptions import IntegrityError, DoesNotExist

from auth import get_session
from game_objects import Player, Game, run
from models import User, init_db, Session, close_db
from schema import ResponseUser, UserCredentials, ResponseGame, GameCredentials, ResponseGamesList, LoginResult

app = FastAPI()
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/token")
log = logging.getLogger(__name__)

app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        'http://localhost:3000',
        'http://127.0.0.1:3000',
        'http://0.0.0.0:3000',
        'http://tgwall.tk:3000',
    ],
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*'],
)

games: Dict[int, Game] = {}


@app.websocket("/game/{game_id}")
async def game_endpoint(ws: WebSocket,
                        game_id: int,
                        session: Session = Depends(get_session),
                        game_password: Optional[str] = Cookie(None)):
    await ws.accept()

    if game_id not in games:
        log.warning(f'Unknown game id: {game_id}')
        await ws.close(code=WS_1008_POLICY_VIOLATION)
        return

    game = games[game_id]

    if game.password and game_password != game.password:
        log.warning(f'Password error')
        await ws.close(code=WS_1008_POLICY_VIOLATION)
        return

    player = Player(ws=ws, user=session.user)

    result = game.add_player(player)
    if not result:
        log.warning('Cant add player to game')
        await ws.close(code=WS_1013_TRY_AGAIN_LATER)
        return

    await game.send()

    try:
        while ws.client_state == WebSocketState.CONNECTED:
            await run(player, game)
    except Exception as e:
        log.error(e, exc_info=True)

    if player.user:
        player.user.score = player.score
        await player.user.save()

    game.remove_player(player)
    if not game.players:
        games.pop(game_id)
    else:
        await game.send()

    log.info(f'{player.name} disconnected.')


@app.post('/create/user', response_model=ResponseUser, status_code=HTTP_201_CREATED)
async def registration(response: Response, credentials: UserCredentials):
    try:
        user = User(name=credentials.name, password=credentials.password)
        await user.save()
    except IntegrityError:
        response.status_code = HTTP_403_FORBIDDEN
        return ResponseUser(
            ok=False,
            error='duplicate'
        )
    except Exception as e:
        response.status_code = HTTP_403_FORBIDDEN
        return ResponseUser(
            ok=False,
            error=str(e)
        )

    return ResponseUser(user={
        "id": user.id,
        "name": user.name,
        "score": user.score,
    })


@app.post('/create/game', response_model=ResponseGame, status_code=HTTP_201_CREATED)
def create_new_game(credentials: GameCredentials, session: Session = Depends(get_session)):
    if not session.user:
        raise HTTPException(status_code=401, detail='Unauthorized players can not create the game')

    game = Game(
        creator=session.user,
        name=credentials.name,
        password=credentials.password or None
    )

    gid = 1
    while True:
        if gid in games:
            gid += 1
        else:
            break

    games.update({gid: game})

    return {
        'id': gid,
        'name': game.name,
        'password': bool(game.password),
        'players': len(game.players)
    }


@app.get("/games", response_model=ResponseGamesList)
def get_rooms(session: Session = Depends(get_session)):
    resp = {
        'ok': True,
        'games': []
    }

    for gid, game in games.items():
        game_json = {
            'id': gid,
            'name': game.name,
            'password': bool(game.password),
            'players': len(game.players)
        }
        resp['games'].append(game_json)

    return resp


@app.post("/login", response_model=LoginResult)
async def login(response: Response, credentials: UserCredentials):
    try:
        user = await User.get(name=credentials.name)
    except DoesNotExist:
        response.status_code = HTTP_403_FORBIDDEN
        return {
            'ok': False,
            'detail': [
                {'msg': 'Such user does not exist'}
            ]
        }

    if user.password != credentials.password:
        response.status_code = HTTP_403_FORBIDDEN
        return {
            'ok': False,
            'detail': [
                {'msg': 'Password error'}
            ]
        }

    session, result = await Session.get_or_create(user=user)
    response.set_cookie(key="session_id", value=str(session.id))
    return LoginResult(session_id=str(session.id))


@app.post("/logout")
async def logout(session: Session = Depends(get_session)):
    await session.delete()
    return True


@app.get("/me", response_model=ResponseUser)
def read_current_user(session: Session = Depends(get_session)):
    user = session.user
    if user:
        return ResponseUser(user={
            "id": user.id,
            "name": user.name,
            "score": user.score,
        })
    return ResponseUser(
        ok=False,
        error='user not found'
    )


@app.on_event("startup")
async def startup():
    await init_db()


@app.on_event("shutdown")
async def startdown():
    await close_db()


if __name__ == '__main__':
    uvicorn.run(app, host='0.0.0.0', port=1984)
