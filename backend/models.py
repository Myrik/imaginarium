import os
from logging import getLogger

from tortoise import fields, Tortoise
from tortoise.models import Model

logger = getLogger(__name__)


class User(Model):
    id = fields.IntField(pk=True)

    name = fields.TextField(unique=True)
    password = fields.TextField(null=True)

    score = fields.IntField(default=0)

    def __str__(self):
        return self.name


class Session(Model):
    id = fields.UUIDField(pk=True)
    user = fields.ForeignKeyField('models.User', null=True)
    created = fields.DatetimeField(auto_now=True)


async def init_db():
    if os.environ.get('DB_TYPE'):
        db_url = '%(DB_TYPE)s://%(POSTGRES_USER)s:%(POSTGRES_PASSWORD)s@%(DB_HOST)s:%(DB_PORT)s/%(POSTGRES_DB)s' % os.environ
    else:
        db_url = 'sqlite://db.sqlite'

    await Tortoise.init(
        db_url=db_url,
        modules={'models': ['models']}
    )
    await Tortoise.generate_schemas()


async def close_db():
    await Tortoise.close_connections()
