import React from "react"
import {Form} from "react-bootstrap"
import Button from "react-bootstrap/Button"
import Modal from "react-modal"
import {api_path} from "../config"
import Cookies from "universal-cookie"
import {Redirect} from "react-router-dom"

const customStyles = {
    content: {
        top: "50%",
        left: "50%",
        right: "auto",
        bottom: "auto",
        marginRight: "-50%",
        transform: "translate(-50%, -50%)",
    },
    overlay: {
        backgroundColor: "#3c414a",
    },
}

class NewGameForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            show: false,
            gid: null,
            name: "",
            password: "",
            connect: null,
        }
    }

    connect_game = (gid) => {
        this.setState({
            connect: {
                id: gid,
                password: this.state.password,
            },
        })
    }

    create_game = () => {
        if(this.state.gid) {
            return this.connect_game(this.state.gid)
        }

        return fetch(
            "http://" + api_path + "/create/game",
            {
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                },
                method: "POST",
                credentials: "include",
                body: JSON.stringify({
                    name: this.state.name,
                    password: this.state.password,
                }),
            },
        ).then(
            result => result.json(),
        ).then(result => {
            console.log(result)
            if(result.id) this.connect_game(result.id)
        })
    }

    handleEnter = (e) => {
        if(e.key === "Enter") {
            this.create_game()
        }
    }

    onAfterOpen = () => {
        if(!this.state.gid) {
            this.nameInput.focus()
        } else {
            this.passwordInput.focus()
        }
    }

    onRequestClose = () => {
        this.setState({
            show: false,
            name: "",
            password: "",
            gid: null,
        })
    }

    render() {
        if(this.state.connect) {
            const cookies = new Cookies()
            cookies.set("game_password", this.state.connect.password, {path: "/"})
            return <Redirect to={"/game/" + this.state.connect.id}/>
        }
        return <Modal
            isOpen={this.state.show}
            style={customStyles}
            contentLabel={this.state.gid ? "Connect to game" : "Create New Game"}
            appElement={document.getElementById("root")}
            onAfterOpen={this.onAfterOpen}
            onRequestClose={this.onRequestClose}
        >
            <Form>
                <Form.Group>
                    <Form.Label>Game name</Form.Label>
                    <Form.Control value={this.state.name}
                                  disabled={this.state.gid}
                                  ref={(input) => {
                                      this.nameInput = input
                                  }}
                                  onKeyDown={this.handleEnter}
                                  onChange={event => {
                                      this.setState({name: event.target.value})
                                  }}/>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password"
                                  value={this.state.password}
                                  ref={(input) => {
                                      this.passwordInput = input
                                  }}
                                  onKeyDown={this.handleEnter}
                                  onChange={event => {
                                      this.setState({password: event.target.value})
                                  }}/>
                </Form.Group>
                <Button onClick={this.create_game} variant="success">{this.state.gid ? "Connect" : "Create"}</Button>
            </Form>
        </Modal>
    }
}

export default NewGameForm
