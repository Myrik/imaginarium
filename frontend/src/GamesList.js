import React from "react"
import {Badge, ListGroup} from "react-bootstrap"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import {faLock, faRedo} from "@fortawesome/free-solid-svg-icons"
import {api_path, cookies} from "./config"
import Button from "react-bootstrap/Button"
import {Redirect} from "react-router-dom"
import NewGameForm from "./modals/newGame"
import ButtonGroup from "react-bootstrap/ButtonGroup"
import Card from "react-bootstrap/es/Card"

const style = {
    listItem: {
        padding: 0,
        fontSize: "1rem",
        userSelect: "none",
        MozUserSelect: "none",
        WebkitUserSelect: "none",
        msUserSelect: "none",
        cursor: "pointer",
        borderRadius: 0,
    },
    cardHeader: {
        color: "black",
        fontSize: "0.7em",
        padding: "5px 10px",
        textAlign: "left",
    },
}



class GamesList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            games: [],
            modalCreateGame: false,
            connect: null,
        }
        this.get_games()
    }

    click = (game) => {
        this.newGameModal.setState({
            gid: game.id,
            name: game.name,
            show: true,
        })
    }

    get_games = () => {
        fetch("http://" + api_path + "/games", {
            credentials: "include",
        }).then(
            data => {
                if(data.status === 200) {
                    return data.json()
                } else if(data.status === 401) {
                    this.logout("code")
                } else {
                    throw new Error("Status error " + data.status)
                }
            },
        ).then(
            data => {
                console.log(data)
                this.setState({"games": data.games})
            },
        ).catch(() => {
        })
    }

    logout = (reason) => {
        console.log("Remove session " + cookies.get("session_id") + " by " + reason)
        if(cookies.get("session_id")) {
            fetch(
                "http://" + api_path + "/logout",
                {
                    credentials: "same-origin",
                    method: "POST",
                },
            ).then(
                () => {
                    cookies.remove("session_id")
                    this.forceUpdate()
                },
            )
        }
    }

    componentDidMount = () => {
        window.addEventListener("keydown", this.handleOnKeyDown)
    }


    componentWillUnmount = () => {
        window.removeEventListener("keydown", this.handleOnKeyDown)
    }

    handleOnKeyDown = (e) => {
        if(e.key === "Escape") {
            this.logout("key")
            e.preventDefault()
        }
    }

    render() {
        if(!cookies.get("session_id")) {
            console.log("Redirect to login")
            return <Redirect to='/login'/>
        }

        if(this.state.connect) {
            cookies.set("game_password", this.state.connect.password, {path: "/"})
            return <Redirect to={"/game/" + this.state.connect.gid}/>
        }

        return <Card>
            <Card.Header style={style.cardHeader}>
                Available games
                <FontAwesomeIcon icon={faRedo}
                                 style={{float: "right", margin: "5px", cursor: "pointer"}}
                                 onClick={this.get_games}
                />
            </Card.Header>

            <ListGroup variant="flush">
                {this.state.games.map((game, idx) => {
                    return <ListGroup.Item variant="primary"
                                           style={style.listItem}
                                           key={idx}
                                           onClick={() => this.click(game)}
                    >
                        {game.name}
                        <Badge variant="secondary" style={{marginLeft: "3px"}}>{game.players}</Badge>
                        {game.password && <FontAwesomeIcon icon={faLock} style={{float: "right", margin: "3px"}}/>}
                    </ListGroup.Item>
                })}
            </ListGroup>
            <ButtonGroup variant='flush'>
                <Button onClick={() => this.newGameModal.setState({show: true})}
                        style={{borderTopLeftRadius: 0}}
                >
                    Create new game
                </Button>
                <Button onClick={this.logout} style={{borderTopRightRadius: 0}}>Logout</Button>
            </ButtonGroup>

            <NewGameForm ref={r => this.newGameModal = r}/>
        </Card>
    }
}

export default GamesList
