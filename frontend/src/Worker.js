import React from "react"
import StatusBox from "./Status"
import Card from "./Card"
import cardSize from "./Card"
import PlayersList, {variants} from "./PlayersList"
import {ListGroup} from "react-bootstrap"
import {Redirect} from "react-router-dom"
import Cookies from "universal-cookie"

const style = {
    width: cardSize[0] * 7 + "px",
    height: cardSize[1] + "px",
    display: "flex",
    justifyContent: "space-around",
}

class Worker extends React.Component {
    cookies = new Cookies()

    constructor(props) {
        super(props)
        let url = "ws://" + props.server + "/game/" + props.gid

        const ws = new WebSocket(url)

        ws.onopen = () => {
            this.state.ws.send(
                JSON.stringify({
                    state: "start",
                }),
            )
        }

        ws.onmessage = event => {
            const msg = JSON.parse(event.data)
            console.log(msg)
            this.on_message(msg)
        }

        ws.onclose = () => {
            console.log("Socket closed")
            this.setState({ws: undefined})
        }

        ws.onerror = (err) => {
            console.log("Socket error: ", err)
        }

        this.state = {
            ws: ws,
            table: [],
            hand: [],
            my_turn: false,
            players: [],
            stage: 0,
        }
        this.textField = React.createRef()
    }

    on_message = (msg) => {
        let my_turn = false
        for(let player of msg.players) {
            if(player.me === true && player.turn === true) {
                my_turn = true
            }
        }
        this.setState({
            ...this.state,
            ...msg,
            my_turn: my_turn,
        })
    }

    hand_click = (event, obj) => {
        console.log("Table click: ", obj.props.value)

        if(this.state.stage === 4) {
            this.state.ws.send("{}")
            return
        }

        const text = this.textField.current.state.text
        if(text || !this.state.my_turn) {
            const data = {
                card: obj.props.value,
                message: text,
            }
            console.log(data)
            this.state.ws.send(JSON.stringify(data))
        } else {
            console.log("Msg is empty")
        }
    }

    table_click = (event, obj) => {
        console.log("Table click: ", obj.props.value)

        if(this.state.stage === 4) {
            this.state.ws.send("{}")
            return
        }

        const card = obj.props.value
        if(card != null) {
            this.state.ws.send(JSON.stringify({
                card: card,
            }))
        }
    }

    componentDidMount = () => {
        window.addEventListener("keydown", this.handleOnKeyDown)
    }


    componentWillUnmount = () => {
        window.removeEventListener("keydown", this.handleOnKeyDown)
    }

    handleOnKeyDown = (e) => {
        console.log(e.key)
        if(e.key === "Escape") {
            this.state.ws.close()
            e.preventDefault()
        }
    }

    render() {
        if(!this.cookies.get("session_id")) {
            console.log("Redirect to login")
            return <Redirect to='/login'/>
        }
        if(!this.state.ws) {
            console.log("Socket closed, redirect")
            return <Redirect to='/games'/>
        }
        return <div>
            <PlayersList players={this.state.players}/>
            <StatusBox text={this.state.message} my_turn={this.state.my_turn} ref={this.textField}/>
            <div style={style}>
                {this.state.table.map((v, n) => {
                    if(this.state.stage === 4) {
                        return <div style={{alignSelf: "flex-end"}} key={n}>
                            <ListGroup>
                                {this.state.votes[v].map((player, idx) => {
                                    return <ListGroup.Item variant={variants[player.color - 1]}
                                                           className="sm"
                                                           key={idx}>
                                        {player.name}
                                    </ListGroup.Item>
                                })}
                            </ListGroup>
                            <Card value={v} key={n} on_click={this.table_click}/>
                        </div>
                    }
                    return <Card value={v} key={n} on_click={this.table_click}/>
                })}
            </div>
            <div style={style}>
                {this.state.hand.map(v => {
                    return <Card value={v} key={Card.numByVal(v)} on_click={this.hand_click}/>
                })}
            </div>
        </div>
    }
}

export default Worker
