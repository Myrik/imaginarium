import React from "react"
import Worker from "./Worker"
import Login from "./Login"
import GamesList from "./GamesList"
import {BrowserRouter, Redirect, Route} from "react-router-dom"
import {api_path} from "./config"

const style = {
    textAlign: "center",
    backgroundColor: "#282c34",
    minHeight: "100vh",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    fontSize: "calc(10px + 2vmin)",
    color: "white",
}

class App extends React.Component {
    render() {
        return <div className="App" style={style}>
            <BrowserRouter>
                <Route exact path="/" render={() => (
                    <Redirect to="/login"/>
                )}/>
                <Route exact path="/login" component={Login}/>
                <Route exact path="/games" component={GamesList}/>
                <Route path="/game/:gid" render={props => (
                    <Worker server={api_path} gid={props.match.params.gid}/>
                )}/>
            </BrowserRouter>
        </div>

    }
}

export default App
