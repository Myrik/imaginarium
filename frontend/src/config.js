import Cookies from "universal-cookie"


let api_server = "localhost"
let api_port = "1984"

const env = window._env_
if(env) {
    api_server = env.API_HOST
    api_port = env.API_PORT
}

export const api_path = api_server + ":" + api_port
export const cookies = new Cookies()
export const variants = ["primary", "secondary", "success", "danger", "warning", "info", "light", "dark"]
export const cards = {
    symbols: ["♣", "♦", "♥", "♠"],
    nums: ["a", "2", "3", "4", "5", "6", "7", "8", "9", "10", "j", "q", "k"],
}
