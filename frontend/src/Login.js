import React from "react"
import {Alert, Button, ButtonGroup, Form} from "react-bootstrap"
import {Redirect} from "react-router-dom"
import {api_path, cookies} from "./config"

class Login extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: "",
            password: "",
            errors: [],
        }
        cookies.remove("session_id")
    }

    parse_errors = result => {
        let errors = []
        for(let err of result.detail) {
            errors.push(err.msg)
        }
        this.setState({errors: errors})
        return errors
    }

    login = (event) => {
        const payload = {
            name: this.state.name,
            password: this.state.password,
        }

        fetch(
            "http://" + api_path + "/login",
            {
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                },
                credentials: "same-origin",
                method: "POST",
                body: JSON.stringify(payload),
            },
        ).then(
            result => result.json(),
        ).then(result => {
            if(result.ok) {
                cookies.set("session_id", result.session_id)
                this.setState({})
                return
            }
            this.parse_errors(result)
        })
    }

    registration = (event) => {
        const payload = {
            name: this.state.name,
            password: this.state.password,
        }

        fetch(
            "http://" + api_path + "/create/user",
            {
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                },
                method: "POST",
                body: JSON.stringify(payload),
            },
        ).then(
            result => result.json(),
        ).then(result => {
            if(result.ok) {
                return this.login(event)
            }
            this.parse_errors(result)
        })
    }

    alert = (text, variant, key) => {
        return <Alert variant={variant}
                      key={key}
                      style={{display: "table", margin: "5px auto", fontSize: "initial", padding: "5px 10px"}}
        >
            {text}
        </Alert>
    }

    componentDidMount = () => {
        window.addEventListener("keydown", this.handleOnKeyDown)
    }


    componentWillUnmount = () => {
        window.removeEventListener("keydown", this.handleOnKeyDown)
    }

    handleOnKeyDown = (e) => {
        if(e.key === "Enter") {
            if(this.state.name && !this.state.password) {
                this.passwordInput.focus()
            } else if(!this.state.name && this.state.password) {
                this.nameInput.focus()
            } else {
                this.login()
            }
            e.preventDefault()
        } else if(e.key === "Escape") {
            this.setState({
                name: "",
                password: "",
            })
            this.nameInput.focus()
            e.preventDefault()
        }
    }

    render() {
        if(cookies.get("session_id")) {
            console.log("Redirect to game")
            return <Redirect to='/games'/>
        }

        return <Form>
            <div style={{position: "absolute", top: "10px", width: "100%", right: 0}}>
                {this.state.errors.map((error, idx) => {
                    return this.alert(error, "danger", idx)
                })}
            </div>
            <Form.Group>
                <Form.Label>Username</Form.Label>
                <Form.Control placeholder="Username"
                              value={this.state.name}
                              onKeyDown={this.handleEnter}
                              ref={(input) => {
                                  this.nameInput = input
                              }}
                              onChange={(event => {
                                  this.setState({name: event.target.value})
                              })}/>
            </Form.Group>
            <Form.Group>
                <Form.Label>Password</Form.Label>
                <Form.Control placeholder="Password"
                              type="password"
                              value={this.state.password}
                              onKeyDown={this.handleEnter}
                              ref={(input) => {
                                  this.passwordInput = input
                              }}
                              onChange={(event => {
                                  this.setState({password: event.target.value})
                              })}/>
            </Form.Group>
            <ButtonGroup vertical>
                <Button onClick={this.login} variant="success">Login</Button>
                <Button onClick={this.registration}>Registration</Button>
            </ButtonGroup>
        </Form>
    }
}

export default Login
