import React from "react"
import svgCards from "./static/svg-cards.svg"
import Radium from "radium"
import {cards} from "./config"


class Card extends React.Component {
    constructor(props) {
        super(props)
        this.on_click = props.on_click || this.on_click
    }

    static valByNum(val) {
        if(Number.isInteger(val)) {
            return cards.nums[(val % 13)] + cards.symbols[val / 4 << 0]
        }
        return val
    }

    static numByVal(val) {
        if(Number.isInteger(val) || val === null) {
            return val
        }

        const symbol = val.slice(-1)
        const num = val.slice(0, -1)

        const rez = {
            symbol: cards.symbols.indexOf(symbol),
            num: cards.nums.indexOf(num.toLowerCase()),
        }

        return rez["symbol"] * 13 + rez["num"]
    }

    getStyle = () => {
        let num = Card.numByVal(this.props.value)

        num = num === null ? 54 : num

        const cardSize = [168, 243]

        const x = (cardSize[0] * num) - num / 2.2 << 0,
            y = cardSize[1] * ((num / 13) << 0)

        const scale = this.props.scale || 1

        return {
            width: cardSize[0] + "px",
            height: cardSize[1] + "px",
            background: "url(" + svgCards + ") -" + x + "px -" + y + "px",
            transition: "all 0.3s ease",
            transform: "scale(" + scale + ")",
            ":hover": {
                transform: "scale(" + scale * 1.1 + ")",
                zIndex: "999",
            },
        }
    }

    on_click = () => {
    }

    render() {
        return <div
            onClick={(ev) => this.on_click(ev, this)}
            style={this.getStyle()}
        >
        </div>
    }
}

Card = Radium(Card)

export default Card
