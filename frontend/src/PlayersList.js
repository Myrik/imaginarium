import React from "react"
import {Badge, ListGroup, Spinner} from "react-bootstrap"

const style = {
    spinner: {
        position: "absolute",
        left: 0,
        margin: "3px",
        padding: 0,
        fontSize: "1rem",
    },
    list: {
        position: "fixed",
        top: "10px",
        right: "10px",
    },
    item: {
        padding: 0,
        fontSize: "1rem",
        width: "300px",
        userSelect: "none",
        MozUserSelect: "none",
        WebkitUserSelect: "none",
        msUserSelect: "none",
    },
    badge: {
        margin: "1px",
        display: "inline-block",
    },
}


export const variants = [
    "primary", "secondary", "success", "danger", "warning", "info", "light", "dark",
]

class PlayersList extends React.Component {
    render() {
        return <ListGroup style={style.list}>
            {this.props.players.map((player, idx) => {
                return <ListGroup.Item variant={variants[player.color - 1]} style={style.item} key={idx}>
                    {player.state && <Spinner animation="border" size="sm" style={style.spinner}/>}
                    <div style={{display: "inline-block", marginRight: "3px"}}>
                        {player.name}
                    </div>
                    <Badge variant="info" style={style.badge}>{player.score}</Badge>
                    {player.me && <Badge style={style.badge} variant="primary">me</Badge>}
                    {player.turn && <Badge style={style.badge} variant="danger">turn</Badge>}
                </ListGroup.Item>
            })}
        </ListGroup>
    }
}

export default PlayersList
