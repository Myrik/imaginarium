import React from 'react';
import {Alert, Form} from 'react-bootstrap';


class StatusBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: props.text || ''
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        if (String.isPrototypeOf(event)) {
            this.setState({text: event});
        } else {
            this.setState({text: event.target.value});
        }
    }

    render() {
        if (this.props.my_turn && !this.props.text) {
            return <Form.Group controlId="message">
                <Form.Label>Message</Form.Label>
                <Form.Control as="textarea" rows="3" onChange={this.handleChange}/>
            </Form.Group>
        }
        if (this.props.text) {
            return <Alert variant="dark">{this.props.text || ''}</Alert>
        }
        return <div/>
    }
}

export default StatusBox;