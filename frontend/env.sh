#!/bin/bash

FILE=$1

# Recreate config file
rm -f "$FILE"
touch "$FILE"
# Add assignment
echo "window._env_ = {" >> "$FILE"

# Read each line in .env file
# Each line represents key=value pairs
printenv | while read line;
do
  # Split env variables by character `=`
  if echo "$line" | grep -q -e '='; then
    varname="${line%%=*}"
    varvalue="${line#*=}"
  fi
  # Read value of current variable if exists as Environment variable
  value="${!varname}"
  # Otherwise use value from .env file
  [[ -z $value ]] && value=${varvalue}

  # Append configuration property to JS file
  echo "  $varname: \"$value\"," >> "$FILE"
done

echo "}" >> "$FILE"
