#!/bin/bash
IMAGE_PATH=registry.gitlab.com/myrik/imaginarium/$1
docker build --no-cache -t $IMAGE_PATH ./$1
docker push $IMAGE_PATH
