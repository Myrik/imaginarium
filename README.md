# Imaginarium
[![pipeline status](https://gitlab.com/Myrik/imaginarium/badges/master/pipeline.svg)](https://gitlab.com/Myrik/imaginarium/commits/master)
[![coverage report](https://gitlab.com/Myrik/imaginarium/badges/master/coverage.svg)](https://gitlab.com/Myrik/imaginarium/commits/master)
## Deploy
  - Create .env file. 
  
    Example:
  
    ```
    DB_TYPE=postgres
    DB_HOST=postgres
    DB_PORT=5432
    
    POSTGRES_USER=any_user
    POSTGRES_PASSWORD=any_password
    POSTGRES_DB=any_db
    
    API_HOST=localhost
    API_PORT=1984
    ```
  - docker-compose up
  
  Frontend will be hosted on http://localhost:3000
  
  API documentation http://localhost:1984/docs

